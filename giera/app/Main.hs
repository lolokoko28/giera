module Main where


import Data.List
import qualified Data.Map.Strict as M
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import System.IO  
import Control.Monad

import Lib
import Grammar
import Parser
import Play


main :: IO ()
main = do
    --putStrLn "Give path to the file"
    --path <- getLine
   -- contents <- withFile "game_config.txt" ReadMode (hGetContents>>=(\x-> return $ parseConfig x))
    handle <- openFile "game_config.txt" ReadMode
    contents <- hGetContents handle
    eGame<-return (parseConfig contents)
    case (eGame) of
        (Right parsedGame) ->putStrLn "Welcome in the game!" >> execStateT play parsedGame >> putStrLn "THE END :)"
        (Left error) -> putStrLn (show error)
    hClose handle  