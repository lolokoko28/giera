module Play where



import Data.List
import qualified Data.Map.Strict as M
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import System.Environment
import System.IO.Error
import System.Random


import Grammar
import Parser
import Help  


commandsStart :: M.Map String (StIO GameState)
commandsStart = M.fromList [
    ("choose", chooseCommand)
  ]

commands :: M.Map String (StIO GameState)
commands = M.fromList [
  ("suicide",   (return $ Ended Loser)),
  ("kill yourself",  (return $ Ended Loser)),
  ("sepuku",   (return $ Ended Loser)),
  ("move", moveCommand >> (return InProgress)),
  ("open door", openDoorCommand >>return InProgress),
  ("fight", fightCommand)
  ]


fightCommand :: StIO GameState
fightCommand = do
  monsters<-getMonsters
  pLoc<-getPlayerLoc
  monster<-return $ findMonster pLoc monsters
  case monster of 
    [] -> lift (putStrLn "There is no monster here to fight with!")   >> return InProgress
    (m:ms)-> fightMonster m

fightMonster :: Monster -> StIO GameState
fightMonster (Monster name a f rn mx my) = do
  player<-getPlayer
  (lift $ putStrLn ("You are fighting with "++ name++ " which force is:" ++(show f))) 
  case player of
    (Player n r m a e)->do
      rand<-getRandom
      (lift $ putStrLn ("Your experience is "++(show e))) 
      if(e+rand>=f) then (
        do
          mon<-getMonsters
          updateMonsters (removeMon name mon)
          monsters <- getMonsters
          case monsters of 
            []->return $ Ended Winner
            otherwise -> return InProgress) 
            else return $ Ended Loser

moveCommand :: StIO ()
moveCommand = ask "which direction? N E S W" >>=(\x-> return $ read x) >>= checkCollision

openDoorCommand :: StIO ()
openDoorCommand = do
  nxy <- getPlayerLoc
  doors <- getDoors
  door<-return $ findDoor nxy doors
  case door of
    [] -> lift (putStrLn "There is no door in here!")
    ((Door nf nt x y xto yto):ds) ->do
      updateState (nt,xto, yto)
      lift (putStrLn ("You went from " ++ nf ++ " to " ++ nt++" now on position (" ++(show xto)++ ", " ++ (show yto)++")!"))

updateState :: (Name, X,Y) -> StIO()
updateState (n,x,y) =do 
  game <-get
  info <- getInfo
  case info of
    (Info i _ _ _)-> put $ Game  (replaceInfo (GInfo $ Info i n x y)  (list game) )

updateMonsters :: [Monster] -> StIO()
updateMonsters ms =do 
  game <-get
  monsters <- getMonsters
  put $ Game  (replaceMonsters (Monsters ms)  (list game) )


checkCollision :: Direction -> StIO ()
checkCollision direction = do
  game <- get 
  gInfo <-return $ head (filter (\x -> case x of
      (GInfo _)->True
      otherwise -> False) (list game))
  case gInfo of
    (GInfo inf) -> case inf of
      (Info iter n x y) -> if not $ (check x y direction) then lift (putStrLn "You cannot move there!") >>moveCommand else 
        do
          put $ Game  (replaceInfo (GInfo $ Info iter n (getX direction x) (getY direction y))  (list game) ) 
          lift (putStrLn ("You moved to new position (" ++(show $ getX direction x)++ ", " ++ (show $ getY direction y)++")" )  )   
    where
      check x y d = case d of
        N -> (y+1)<heightRoom 
        S -> (y-1)>=0
        E -> (x+1)<widthRoom
        W -> (x-1)>=0
      getX d x= case d of
        E->x+1
        W->x-1
        otherwise -> x
      getY d y= case d of
        N->y+1
        S->y-1
        otherwise -> y
  
replaceInfo :: Section -> [Section] -> [Section]
replaceInfo _ [] = []
replaceInfo i (x:xs) = case x of
  (GInfo _) -> i:xs
  otherwise -> x:(replaceInfo i xs)

replaceMonsters :: Section -> [Section] -> [Section]
replaceMonsters _ [] = []
replaceMonsters m (x:xs) = case x of
  (Monsters _) -> m:xs
  otherwise -> x:(replaceMonsters m xs)

chooseCommand :: StIO GameState
chooseCommand = do
  printPlayers
  name<-ask "Which Player do you want to play? Give the name of this player" 
  choosePlayer name

readCommand :: GameState-> M.Map String (StIO GameState)->StIO GameState
readCommand gs cmds = lift getLine >>= (processCommand gs cmds)

processCommand ::GameState->M.Map String (StIO GameState)-> String -> StIO GameState
processCommand gs cmds cmd= M.findWithDefault (unknownCommand gs) cmd cmds

unknownCommand :: GameState-> StIO GameState
unknownCommand gs =  (lift $ putStrLn "Unknown command") >> return gs

startLoop :: StIO ()
startLoop = do
  result <- readCommand Start commandsStart
  case (result) of 
    (Start) -> startLoop
    otherwise -> return ()

mainLoop :: StIO ()
mainLoop = do
  result <- readCommand InProgress commands
  case (result) of
    (InProgress) ->  mainLoop 
    (Ended Winner) -> (lift $ putStrLn "WOW YOU WON!") >>return ()
    (Ended Loser) -> (lift $ putStrLn "YOU ARE A LOSER! BYE BYE") >>return ()
    otherwise -> return ()

play :: StIO ()
play = startLoop >> mainLoop