module Grammar where



data Game = Game [Section] deriving Show
data Section = Stories [Story] | Rooms [Room]| Players [Player] | Monsters [Monster] | Items [Item] | Events [Event] | Doors [Door] | GInfo Info deriving Show
data Story = Story Description Index deriving Show
data Player = Player Name Race MaxHealth ActualHealth Exp deriving Show 
data Monster = Monster Name ActualHealth Force Name X Y deriving Show
data Room = Room Name Description deriving Show
data Item = Item Name Type deriving Show
data Type = HeavyItem | LightItem deriving (Show, Read)
data Event = Event Name Description Index deriving Show
data Race = Human | Potato deriving (Show, Read)
data Info = Info Iterations Name X Y deriving (Show, Read)
data GameState = Start | InProgress | Ended Result deriving (Show, Read)
data Result = Winner | Loser deriving (Show,Read)
data Door = Door RoomFromName RoomToName X Y X Y deriving Show
type Index = Int
type Iterations = Int
type Force = Int
type Exp = Int 
type MaxHealth = Int 
type ActualHealth = Int 
type Height = Int
type Width = Int
type X = Int
type Y = Int
type RoomFromName = String
type RoomToName = String
type Description = String
type Name = [Char] 

