module Help where


import Data.List
import qualified Data.Map.Strict as M
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.State.Strict
import System.Environment
import System.IO.Error
import System.Random
import Grammar
import Parser

data Direction = N|E|S|W deriving (Show, Read)
type StIO a = StateT Game IO a


fst3 :: (a,b,c)->a
fst3 k = case k of
   (a,b,c) -> a

snd3 :: (a,b,c)->b
snd3 k = case k of 
  (a,b,c) -> b

thd3 :: (a,b,c)->c
thd3 k = case k of 
  (a,b,c) -> c

getRandom::StIO Int
getRandom = lift (randomRIO (0,10))



removeMon :: Name -> [Monster] -> [Monster]
removeMon n ((Monster mn a f rn mx my):xs) = if mn == n then xs else ((Monster mn a f rn mx my):(removeMon n xs))

findMonster:: (Name, X, Y) -> [Monster] -> [Monster]
findMonster _ [] = []
findMonster (n,x,y) ((Monster mn a f rn mx my):ds) = if n==rn && x==mx && y==my  then (Monster mn a f rn mx my):[] else findMonster (n,x,y) ds


findDoor :: (Name, X, Y) -> [Door] -> [Door]
findDoor nxy [] = []
findDoor nxy ((Door nf nt x y xt yt):ds) = if (fst3 nxy)==nf && x==(snd3 nxy) && y==(thd3 nxy)   then (Door nf nt x y xt yt):[] else findDoor nxy ds


getPlayer :: StIO Player
getPlayer = do
  game <- get 
  players <-return $ head (filter (\x -> case x of
      (Players _)->True
      otherwise -> False) (list game))
  case players of
    (Players p) -> return $ head p

getPlayerLoc :: StIO (Name, X, Y)
getPlayerLoc = do
  game <- get 
  gInfo <-return $ head (filter (\x -> case x of
      (GInfo _)->True
      otherwise -> False) (list game))
  case gInfo of
    (GInfo inf) -> case inf of
      (Info _ n x y) -> return (n,x,y)

getRooms :: StIO [Room]
getRooms = do
  game <- get 
  rooms <-return $ head (filter (\x -> case x of
    (Rooms _)->True
    otherwise -> False) (list game))
  case rooms of
    (Rooms r) -> return r

getMonsters :: StIO [Monster]
getMonsters = do
  game <- get 
  monsters <-return $ head (filter (\x -> case x of
    (Monsters _)->True
    otherwise -> False) (list game))
  case monsters of
    (Monsters ms) -> return ms

getDoors :: StIO [Door]
getDoors = do
  game <- get 
  doors <-return $ head (filter (\x -> case x of
    (Doors _)->True
    otherwise -> False) (list game))
  case doors of
    (Doors d) -> return d

getInfo :: StIO Info
getInfo = do
  game <- get 
  gInfo <-return $ head (filter (\x -> case x of
    (GInfo _)->True
    otherwise -> False) (list game))
  case gInfo of
    (GInfo inf) -> return inf


list :: Game-> [Section]
list g = case g of 
  (Game l) -> l
   
ask :: String -> StIO String
ask prompt = lift (putStrLn prompt >> getLine)

  
printPlayers :: StIO ()
printPlayers = do
  lift $ putStrLn "Players:"
  game<-get
  case (game) of
    (Game list) -> (lift $ putStrLn $ show (findP list))  >> return ()

findP :: [Section] -> [Player]
findP [] = []
findP (x:xs)= case x of 
  (Players p) ->  p
  otherwise -> findP xs

choosePlayer :: Name -> StIO GameState
choosePlayer name = do 
  g<- get
  case g of
    (Game list) -> if (findPlayer name list) then (put $ Game (changePlayers name list)) >> return InProgress else return Start

changePlayers :: Name-> [Section]->[Section]
changePlayers name (x:xs) = case (x) of
  (Players list ) -> (Players $ leaveOnly name list):xs
  otherwise -> x:(changePlayers name xs)

findPlayer :: Name->[Section]->Bool
findPlayer name (x:xs) = case (x) of
  (Players list ) -> checkPlayer name list
  otherwise -> (findPlayer name xs)

checkPlayer :: Name -> [Player]-> Bool
checkPlayer _ [] = False
checkPlayer name ((Player n r _ _ _):ps) = if n==name then True else (checkPlayer name ps)

leaveOnly :: Name -> [Player]-> [Player]
leaveOnly _ [] = []
leaveOnly name ((Player n r m a e):ps) = if n==name then [(Player n r m a e)] else (leaveOnly name ps)


