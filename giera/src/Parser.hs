module Parser where

import Text.ParserCombinators.Parsec
import Text.Parsec.Token
import Text.Read
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

import Grammar

configFile :: Parser Game
configFile = spaces >> (many section)<* eof >>= (\x->return $ (Game x)) 

consumeSE:: Parser String
consumeSE = char '<'>> ((string "Start") <|> (string "End")) <* char '>' <*spaces

consumeToken::String ->  Parser String
consumeToken token= string token <*spaces 

section :: Parser Section
section = do
        consumeSE
        section <- (fmap Players insidePlayers) <|> (fmap Monsters insideMonsters) <|> (fmap Rooms insideRooms) <|> (fmap Events insideEvents) <|> (fmap Items insideItems) <|>(fmap Stories insideStories) <|>(fmap Doors insideDoors)<|>(fmap GInfo insideInfo)
        consumeSE
        return section

insideDoors :: Parser [Door]
insideDoors = inside "Doors" door

insideMonsters :: Parser [Monster]
insideMonsters = inside "Monsters" monster

insidePlayers :: Parser [Player]
insidePlayers = inside "Players" player

insideRooms :: Parser [Room]
insideRooms = inside "Rooms" room

insideEvents :: Parser [Event]
insideEvents = inside "Events" event

insideItems :: Parser [Item]
insideItems = inside "Items" item

insideStories :: Parser [Story]
insideStories = inside "Stories" story

insideInfo :: Parser Info
insideInfo = consumeToken "GInfo" >> info


info :: Parser Info
info = do 
    consumeToken "Info"
    iter <- readLabel <* spaces
    roomName <- getLabel <* spaces
    x <- readLabel <* spaces
    y<- readLabel <* spaces
    return $ Info iter roomName x y

inside :: String->Parser a -> Parser [a]
inside token p=consumeToken token >> many p

door::Parser Door
door = do 
    consumeToken "Door"
    nameFrom <- getLabel <* spaces
    nameTo <- getLabel <* spaces
    x <- readLabel <* spaces
    y<- readLabel <* spaces
    xto <- readLabel <* spaces
    yto<- readLabel <* spaces
    return $ Door nameFrom nameTo x y xto yto

story :: Parser Story
story = do 
    consumeToken "Story"
    des <- description <* spaces
    index <- readLabel <* spaces
    return $ Story des index

item::Parser Item
item = do
    consumeToken "Item"
    name <- getLabel <* spaces
    itemType <- readLabel <* spaces
    return $ Item name itemType

event :: Parser Event
event = do 
    consumeToken "Event"
    name <- getLabel <*spaces
    des <- description<*spaces
    index <- readLabel <* spaces
    return $ Event name des index

room :: Parser Room
room = do
    consumeToken "Room"
    name <- getLabel <*spaces
    des <- description<*spaces
    return $ Room name des

monster :: Parser Monster
monster =do
    consumeToken "Monster"
    name <- getLabel <* spaces
    actualh <- readLabel <* spaces
    force <- readLabel <* spaces
    rname <- getLabel <*spaces
    x<-readLabel <* spaces
    y<-readLabel <* spaces
    return $ Monster name actualh force rname x y

player :: Parser Player
player= do
    consumeToken "Player"
    name <- getLabel <* spaces
    race <- readLabel <* spaces
    maxh <- readLabel <* spaces
    actualh <- readLabel <* spaces
    exp <- readLabel <* spaces
    return $ Player name race maxh actualh exp

description ::Parser String
description = char '<'>>many (noneOf ">")<*char '>' 

--parses name
getLabel :: Parser String
getLabel = many (noneOf " \n")

readLabel ::(Read a)=>  Parser a
readLabel = fmap read getLabel

widthRoom :: Int
widthRoom = 4

heightRoom :: Int
heightRoom = 4

getAllArgs :: Parser [String]
getAllArgs = many (getLabel <* spaces)

parseConfig :: String -> Either ParseError Game
parseConfig input = parse configFile "an error occured" input